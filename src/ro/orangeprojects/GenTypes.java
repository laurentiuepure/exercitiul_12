package ro.orangeprojects;

public class GenTypes <T1, T2>{
    T1 name;
    T2 value;

    GenTypes (T1 name, T2 value){ // definim constructorul
        this.name = name;
        this.value = value;
    }

    // apoi cream cele doua gettere
    public T1 getName() {
        return name;
    }

    public T2 getValue() {
        return value;
    }
}
